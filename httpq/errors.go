package httpq

import (
	"encoding/json"
	"net/http"
)

type httpqError struct {
	Status  int    `json:"-"`     // Kind of HTTP status this error use
	Message string `json:"error"` // Error message
	err     error  // a sub error
}

func (e *httpqError) Error() string {
	if e.err != nil {
		return e.Error()
	}
	return e.Message
}

func (e *httpqError) json() string {
	b, err := json.Marshal(e)
	if err != nil {
		panic("Failed to convert error message to JSON")
	}
	return string(b)
}

var (
	errUnsafeGet    = newError("Request was not idempotent. You cannot perform UPDATE, INSERT or DELETE queries via GET.", http.StatusBadRequest)
	errNoQuery      = newError("There was no query supplied with the request.", http.StatusBadRequest)
	errInvalidQuery = newError("There was problem parsing the SQL query.", http.StatusBadRequest)
	Forbidden       = newError("Your credentials were not accepted", http.StatusForbidden)
)

func newError(msg string, status int) *httpqError {
	e := &httpqError{Message: msg, Status: status}
	if e.Status < 1 {
		e.Status = http.StatusInternalServerError
	}
	return e
}

func wrapError(err error) *httpqError {
	if herr, ok := err.(*httpqError); ok {
		return herr
	}
	e := newError(err.Error(), http.StatusInternalServerError)
	e.err = err
	return e
}
