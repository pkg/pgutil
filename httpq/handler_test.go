package httpq

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"

	"bitbucket.org/pkg/pgutil/rewrite"
	"database/sql"
	_ "github.com/lib/pq"
)

type testCase struct {
	Method           string
	Query            string
	QueryArgs        []interface{}
	ExpectedResponse string
	ExpectedStatus   int
}

var ts *httptest.Server

var clean = regexp.MustCompile(`(?m:^\s+|\s+$)`)

// These are end to end tests.
// You must have a postgres database (pqutil_test) setup and listening
// You can use the PGUSER etc environment variables to configure.
// See test.sh for bootstrapping.

// ------------------

type Session struct {
	MemberId string
	Admin    bool
}

func panicif(err error) {
	if err != nil {
		panic(err)
	}
}

func (s *Session) Schema() (schema *rewrite.Schema, err error) {

	schema = rewrite.NewSchema()

	v, _ := schema.Add("member", `
		SELECT id,name 
		FROM private.member 
		WHERE id IN (SELECT member_id FROM organisation_member)`)
	v.Grant(rewrite.Update, "name")

	v, _ = schema.Add("organisation_member", `
		SELECT member_id, organisation_id
		FROM private.organisation_member
		WHERE organisation_id IN (SELECT id FROM organisation)`)

	v, _ = schema.Add("organisation", `
		SELECT id, name 
		FROM private.organisation 
		WHERE id IN (
		 	SELECT id 
		 	FROM private.organisation, private.organisation_member 
		 	WHERE organisation_id = organisation.id AND member_id = $1)`, s.MemberId)
	v.Grant(rewrite.Update, "organisation_id", "name")

	v, _ = schema.Add("document", `
		SELECT id, organisation_id, name 
		FROM private.document 
		WHERE organisation_id IN (SELECT id FROM organisation)`)
	v.Grant(rewrite.Update, "organisation_id", "name")

	return schema, nil
}

func getRw(r *http.Request) (rewrite.Rewriter, error) {
	session := &Session{MemberId: r.URL.Query().Get("member_id")}
	schema, err := session.Schema()
	if err != nil {
		return nil, err
	}
	return schema, nil
}

func init() {
	// connect to postgres
	db, err := sql.Open("postgres", "dbname=pqutil_test sslmode=disable")
	if err != nil {
		panic(err)
	}
	mux := http.NewServeMux()
	mux.Handle("/simple/", NewSimpleHandler(db))
	h := NewHandler(db, getRw)
	api := h.(*RewriteHandler)
	// api.Debug = true
	mux.Handle("/api/", api)

	ts = httptest.NewServer(mux) // start a test server using httpq handlers
}

// ------------------

func doRequest(method, path string, b []byte) (status int, body []byte) {
	postReader := bytes.NewReader(b)
	r, err := http.NewRequest(method, ts.URL+path, postReader)
	if err != nil {
		panic(err)
	}
	client := &http.Client{}
	res, err := client.Do(r)
	if err != nil {
		panic(err)
	}
	status = res.StatusCode
	body, err = ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		panic(err)
	}
	return
}

func checkResp(t *testing.T, path string, tc *testCase) {
	q := &query{Query: tc.Query, Values: tc.QueryArgs}
	path += "?member_id=1"
	if q.Query != "" {
		path = path + "&q=" + q.URLEncode()
	}
	if tc.Method == "" {
		tc.Method = "GET"
	}
	status, body := doRequest(tc.Method, path, q.json())
	if status != tc.ExpectedStatus {
		t.Fatalf("Expected to get response status %d but got %d\nResponse was: %s", tc.ExpectedStatus, status, string(body))
	}

	var responseData interface{}
	err := json.Unmarshal(body, &responseData)
	if err != nil {
		t.Fatalf("Could not decode JSON in response body: %v\n-----%s\n--------", err, string(body))
	}
	responseJson, err := json.Marshal(responseData)

	var expectedData interface{}
	err = json.Unmarshal([]byte(tc.ExpectedResponse), &expectedData)
	if err != nil {
		t.Fatalf("Could not decode JSON in ExpectedResponse field in the test case: %v\n%s", err, tc.ExpectedResponse)
	}
	expectedJson, err := json.Marshal(expectedData)

	if string(responseJson) != string(expectedJson) {
		t.Fatalf("Server response did not match what was expected.\n\nRequest:GET %s\n\nResponse:\n%s\n\nExpected:\n%s", path, responseJson, expectedJson)
	}
}

func TestSimpleHandler(t *testing.T) {
	checkResp(t, `/simple/`, &testCase{
		Query:          "SELECT id, name FROM private.member",
		QueryArgs:      nil,
		ExpectedStatus: 200,
		ExpectedResponse: `[{"id":1,"name":"alice"},
		  					{"id":2,"name":"bob"},
		  					{"id":3,"name":"jeff"}]`,
	})
}

func checkApi(t *testing.T, tc *testCase) {
	checkResp(t, "/api/", tc)
}

func TestSelectMember(t *testing.T) {
	checkApi(t, &testCase{
		Method:         "GET",
		Query:          "SELECT name FROM member",
		QueryArgs:      nil,
		ExpectedStatus: 200,
		ExpectedResponse: `[{"name":"alice"},
							{"name":"bob"}]`,
	})
}

func TestSelectDocuments(t *testing.T) {
	checkApi(t, &testCase{
		Method:         "GET",
		Query:          "SELECT name FROM document",
		QueryArgs:      nil,
		ExpectedStatus: 200,
		ExpectedResponse: `[{"name":"Secret SuperGloboCorp document 1"},
		  					{"name":"Secret SuperGloboCorp document 2"}]`,
	})
}

func TestSelectDocument(t *testing.T) {
	checkApi(t, &testCase{
		Method:           "GET",
		Query:            "SELECT name FROM document WHERE id = $1",
		QueryArgs:        []interface{}{2},
		ExpectedStatus:   200,
		ExpectedResponse: `[{"name":"Secret SuperGloboCorp document 2"}]`,
	})
}

func TestUpdateReturnsErrorsOnGET(t *testing.T) {
	checkApi(t, &testCase{
		Method:           "GET",
		Query:            "UPDATE document SET name = $1",
		QueryArgs:        []interface{}{"THIS SHOULD FAIL"},
		ExpectedStatus:   400,
		ExpectedResponse: errUnsafeGet.json(),
	})
}

func TestUpdate(t *testing.T) {
	checkApi(t, &testCase{
		Method:           "POST",
		Query:            "UPDATE document SET name = $1 WHERE id = $2",
		QueryArgs:        []interface{}{"RENAMED", 3},
		ExpectedStatus:   200,
		ExpectedResponse: `{"count": 1}`,
	})
}
