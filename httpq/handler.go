package httpq

import (
	"bitbucket.org/pkg/pgutil/parser"
	"bitbucket.org/pkg/pgutil/rewrite"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/http"
	"strings"

	"database/sql"
	_ "github.com/lib/pq"
)

func recast(arg interface{}) interface{} {
	switch v := arg.(type) {
	case float64:
		// JSON decoding will turn all numbers into float64.
		// This will make postgres pretty unhappy for the common
		// cases of integers (since '1.0'::int is not a valid cast).
		// So to avoid this we'll check if any of the float64s can
		// be represented safely as int64s, and if so convert them
		if math.Floor(v) == v {
			arg = int64(v)
		}
	}
	return arg
}

// Type representing a query request
type query struct {
	Query  string        `json:"query"`
	Values []interface{} `json:"values"`
}

// Return query suitable for placing in a URL querystring
func (q *query) URLEncode() string {
	return base64.URLEncoding.EncodeToString(q.json())
}

// Return JSON representation
func (q *query) json() []byte {
	b, err := json.Marshal(q)
	if err != nil {
		panic(err)
	}
	return b
}

// Unmarshal JSON
func (q *query) Decode(r io.Reader) (err error) {
	err = json.NewDecoder(r).Decode(&q)
	if err != nil {
		return
	}
	for i, _ := range q.Values {
		q.Values[i] = recast(q.Values[i])
	}
	return
}

func isIdempotent(q string) (bool, error) {
	e, err := parser.Parse(q)
	if err != nil {
		return false, err
	}
	if e.Is(parser.Update) || e.Is(parser.Insert) || e.Is(parser.Delete) {
		return false, nil
	}
	for _ = range e.Find(parser.Update, true) {
		return false, nil
	}
	for _ = range e.Find(parser.Insert, true) {
		return false, nil
	}
	for _ = range e.Find(parser.Delete, true) {
		return false, nil
	}
	return true, nil
}

// Extract the SQL query and args from the request.
func getQuery(r *http.Request) (_ *query, err error) {
	var rd io.Reader = r.Body
	var q = &query{}
	if r.Method == "GET" {
		if _, ok := r.URL.Query()["q"]; ok {
			rd = strings.NewReader(r.URL.Query().Get("q"))
			rd = base64.NewDecoder(base64.URLEncoding, rd)
		}
	}
	if rd == nil {
		return nil, errNoQuery
	}
	err = q.Decode(rd)
	switch {
	case err == io.EOF:
		return nil, errNoQuery
	case err != nil:
		return
	}
	if r.Method == "GET" {
		ok, err := isIdempotent(q.Query)
		if err != nil {
			return nil, errInvalidQuery
		}
		if !ok {
			return nil, errUnsafeGet
		}
	}
	return q, nil
}

// Simple error reporting from handlers
func errorHandler(w http.ResponseWriter, err error) {
	e, ok := err.(*httpqError)
	if !ok {
		e = wrapError(err)
	}
	w.WriteHeader(e.Status)
	enc := json.NewEncoder(w)
	enc.Encode(e)
}

// A function type called by RewriteHandler to fetch the rewrite.Rewriter to apply
type RewriteFunc func(r *http.Request) (rewrite.Rewriter, error)

// The handler accepts an SQL query via the "q" request param
// and returns a JSON response.
//
// The query API is very simple. POST a JSON object with a "q" a string for the SQL query,
// and [an optional] "values" array which is a list of arguments to bind to any placeholders
// in the query ($1, $2.. etc).
//
// For example:
//
// {
// 	"q": "SELECT id,name,age FROM thing WHERE age > $1 AND age < $2",
// 	"values": [1,100]
// }
//
// You'll receive back a JSON response.
//
// It is sometimes desirable to make SELECT queries as GET requests, primarily to take advantage
// of HTTP caches. Do do this you must encode the query json using base64 encoding (The URL-safe
// version specified in RFC 4648) and pass it as a `q` querystring parameter. It is an error to
// make an UPDATE, INSERT or DELETE request via GET.
type RewriteHandler struct {
	Debug bool
	db    *sql.DB
	rwf   RewriteFunc
}

// Implement http.Handler
func (h *RewriteHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	req, err := getQuery(r)
	if err != nil {
		errorHandler(w, err)
		return
	}
	if req.Query == "" {
		errorHandler(w, fmt.Errorf("No query SQL supplied"))
		return
	}
	// Dump query debugging
	if h.Debug {
		fmt.Printf("Requested: %s\n", req.Query)
		for i, v := range req.Values {
			fmt.Printf("arg %d -> %v %T\n", i, v, v)
		}
	}
	// Rewrite SQL
	var rw rewrite.Rewriter = rewrite.JSON
	if h.rwf != nil {
		rw2, err := h.rwf(r)
		if err != nil {
			errorHandler(w, err)
			return
		}
		rw = rewrite.Chain(rw2, rw)
	}
	q, args, err := rw.Rewrite(req.Query, req.Values...)
	if err != nil {
		errorHandler(w, err)
		return
	}
	// Dump query debugging
	if h.Debug {
		fmt.Printf("Rewritten: %s\n", q)
		for i, v := range args {
			fmt.Printf("arg %d -> %v %T\n", i, v, v)
		}
	}
	// Fetch JSON
	var json sql.NullString
	err = h.db.QueryRow(q, args...).Scan(&json)
	if err != nil {
		errorHandler(w, err)
		return
	}
	fmt.Fprintln(w, json.String)
}

// Create a new http.Handler that extracts an SQL query from the response,
// rewrites it using the rewrite.Rewriter returned from the supplied
// func, makes a query to the given db and finally returns the
// response in JSON format.
func NewHandler(db *sql.DB, rwf RewriteFunc) http.Handler {
	return &RewriteHandler{db: db, rwf: rwf}
}

// Helper function for when you just want to pass one (or a chain of)
// rewrite.Rewriters and have them always execute.
func NewSimpleHandler(db *sql.DB, rws ...rewrite.Rewriter) http.Handler {
	return &RewriteHandler{
		db: db,
		rwf: func(r *http.Request) (rewrite.Rewriter, error) {
			return rewrite.Chain(rws...), nil
		},
	}
}
