package rewrite

import (
	"bitbucket.org/pkg/pgutil/parser"
	"fmt"
)

type permission uint8

const (
	Select permission = 0
	Update permission = 1 << iota
	Insert
	Delete
)

// Schema is a Rewriter. It lets you build a "soft" view of the database
// schema to restrict what queries can be made.
type Schema struct {
	Views map[string]*View
}

func NewSchema() (s *Schema) {
	s = &Schema{}
	s.Views = make(map[string]*View)
	return s
}

func (s *Schema) Add(name, sql string, args ...interface{}) (v *View, err error) {
	v, err = NewView(sql, args...)
	if err != nil {
		return
	}
	s.Views[name] = v
	return
}

// Returns list of CTEs for a view (and it's dependencies)
// Updates included and returns new ctes list
func (s *Schema) ctes(name string, included map[string]bool, ctes []*parser.Expr, args []interface{}) ([]*parser.Expr, []interface{}) {
	if included[name] {
		return ctes, args
	}
	included[name] = true
	v, ok := s.Views[name]
	if !ok {
		// A missing view here is a real table not an error
		return ctes, args
	}
	for _, dep := range v.e.Find(parser.Source, true) {
		if !dep.Is(parser.Table) {
			continue
		}
		if dep.Has(parser.QualifiedName) {
			continue
		}
		ctes, args = s.ctes(dep.GetValue(), included, ctes, args)
	}
	e := v.e.Clone()
	e.Tag(parser.Source)
	cte := parser.NewExpr(parser.WithSource)
	cte.SetValue(name)
	cte.Append(e)
	shiftBindings(cte, len(args))
	return append(ctes, cte), append(args, v.args...)
}

func (s *Schema) Rewrite(q string, args ...interface{}) (_ string, _ []interface{}, err error) {
	e, err := parser.Parse(q)
	if err != nil {
		return "", nil, err
	}
	// Rewrite target table
	if e.Is(parser.Insert) || e.Is(parser.Update) || e.Is(parser.Delete) {
		table, er := e.Get(parser.Table)
		if er != nil {
			err = er
			return
		}
		target := table.GetValue()
		v, ok := s.Views[target]
		if !ok {
			err = fmt.Errorf("Target relation '%s' does not exist", target)
			return
		}
		// Check field write permissions.
		switch {
		case e.Is(parser.Delete):
			if !v.Has(Delete) {
				err = fmt.Errorf("You do not have permission to Delete '%s'", target)
				return
			}
		case e.Is(parser.Insert):
			if !v.Has(Insert) {
				err = fmt.Errorf("You do not have permission to Insert into '%s'", target)
				return
			}
			for _, e := range e.Find(parser.Column, false) {
				field := e.GetValue()
				if !v.Has(Insert, field) {
					err = fmt.Errorf("You do not have permission to Insert field '%s' in '%s'", field, target)
					return
				}
			}
		case e.Is(parser.Update):
			for _, e := range e.Find(parser.Assignment, false) {
				e, err = e.Get(parser.Column)
				if err != nil {
					return
				}
				field := e.GetValue()
				if !v.Has(Update, field) {
					err = fmt.Errorf("You do not have permission to Update field '%s' in '%s'", field, target)
					return
				}
			}
		}
		// Rewrite the target table
		if realTable, err := v.target(); err != nil {
			return "", args, err
		} else {
			if !e.Replace(parser.Table, realTable) {
				panic("Expected to replace table expression")
			}
		}
	}
	// Add dependent CTE statements if required
	ctes := make([]*parser.Expr, 0)
	cargs := make([]interface{}, 0)
	included := make(map[string]bool)
	for _, src := range e.Find(parser.Source, true) {
		if !src.Is(parser.Table) {
			continue
		}
		ctes, cargs = s.ctes(src.GetValue(), included, ctes, cargs)
	}
	shiftBindings(e, len(cargs))
	for i := len(ctes) - 1; i >= 0; i-- {
		e = prependCTE(e, ctes[i])
	}
	return e.String(), append(cargs, args...), err
}

// A "soft" view of a database relation. Much like postgres native views, a View
// is a made up of an SQL statement that returns the data that is visible under
// that name.
type View struct {
	e    *parser.Expr
	args []interface{}
	cols map[string]permission
	tab  permission
}

func NewView(sql string, args ...interface{}) (v *View, err error) {
	v = &View{}
	v.cols = make(map[string]permission)
	err = v.parse(sql, args)
	return
}

func (v *View) parse(sql string, args []interface{}) (err error) {
	v.e, err = parser.Parse(sql)
	if err != nil {
		return err
	}
	for _, e := range v.e.Find(parser.Column, false) {
		name := e.GetValue()
		v.cols[name] = Select
	}
	v.args = args
	return nil
}

// Get the name of the target table expression.
// If the view is not constructed from a single relation source (ie is a JOIN or SELECT source)
// then this will return an error.
// You may only INSERT or UPDATE simple views.
func (v *View) target() (e *parser.Expr, err error) {
	sources := v.e.Find(parser.Table, false)
	if len(sources) != 1 {
		return nil, fmt.Errorf("Cannot UPDATE, INSERT or DELETE from a view with multiple or mixed sources")
	}
	return sources[0], nil
}

// Grant permission to one or more columns.
// Insert and Delete permissions affect the entire view.
func (v *View) Grant(p permission, cols ...string) error {
	for _, name := range cols {
		old, ok := v.cols[name]
		if !ok {
			return fmt.Errorf("Cannot grant permission to unknown column '%s'", name)
		}
		v.cols[name] = old | p
	}
	if p&(Insert|Delete) != 0 {
		v.tab = v.tab | p
	}
	return nil
}

// Check for a permission.
// Optionally supplied column names will check permissions per column.
// Will return false if one or more of the column names do not exist in the view.
func (v *View) Has(p permission, cols ...string) bool {
	for _, name := range cols {
		old, ok := v.cols[name]
		if !ok {
			return false
		}
		if old&p == 0 {
			return false
		}
	}
	if p&(Insert|Delete) != 0 {
		if v.tab&p == 0 {
			return false
		}
	}
	return true
}

// Output view SQL as string
func (v *View) String() string {
	return v.e.String()
}
