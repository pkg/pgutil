package rewrite

type RewriteChain struct {
	rws []Rewriter
}

func (rw *RewriteChain) Rewrite(sql string, args ...interface{}) (_ string, _ []interface{}, err error) {
	for _, rw := range rw.rws {
		sql, args, err = rw.Rewrite(sql, args...)
		if err != nil {
			return sql, args, err
		}
	}
	return sql, args, nil
}

// Returns a new Rewriter that calls each rw in the list.
// If the list is empty the rewriter will just return the original.
func Chain(rws ...Rewriter) (rw Rewriter) {
	return &RewriteChain{rws: rws}
}
