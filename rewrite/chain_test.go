package rewrite

import (
	"testing"
)

type testrw struct {
}

func (rw *testrw) Rewrite(insql string, args ...interface{}) (outsql string, outargs []interface{}, err error) {
	outsql = insql + "X"
	outargs = append(args, 1)
	return
}

func TestEmptyChain(t *testing.T) {
	rw := Chain()
	q, args, err := rw.Rewrite("OK", 1)
	if err != nil {
		t.Fatal(err)
	}
	if q != "OK" {
		t.Fatalf("Empty chain should leave the query unaffected but got: %s", q)
	}
	if len(args) != 1 {
		t.Fatalf("Empty chain should leave the args unaffected but got %d more args than expected", len(args)-1)
	}
}

func TestChain(t *testing.T) {

	rw := Chain(&testrw{}, &testrw{}, &testrw{})
	q, args, err := rw.Rewrite("")
	if err != nil {
		t.Fatal(err)
	}
	if q != "XXX" {
		t.Fatalf("Chain of 3x Rewriters should have been XXX but got: %s", q)
	}
	if len(args) != 3 {
		t.Fatalf("Chain of 3x Rewriters should have added 3 args but got %d", len(args))
	}
}
