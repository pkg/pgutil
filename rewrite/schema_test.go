package rewrite

import "testing"

func fatalRewrite(t *testing.T, insql, outsql, expsql string, msg ...string) {
	if len(msg) == 0 {
		msg = []string{"Did not get expected result of rewrite"}
	}
	t.Fatalf("\nInput   : %s\nOutput  : %s\nExpected: %s\nMsg     : %s", insql, outsql, expsql, msg[0])
}

func checkRewrite(t *testing.T, schema *Schema, insql, expsql string, args ...interface{}) (string, []interface{}) {
	outsql, args, err := schema.Rewrite(insql, args...)
	if err != nil {
		fatalRewrite(t, insql, outsql, expsql, err.Error())
	}
	if insql == outsql {
		fatalRewrite(t, insql, outsql, expsql, "Did not rewrite query at all!")
	}
	if outsql != expsql {
		fatalRewrite(t, insql, outsql, expsql)
	}
	return outsql, args
}

func checkDenyRewrite(t *testing.T, schema *Schema, insql string) {
	outsql, _, err := schema.Rewrite(insql)
	if err == nil {
		fatalRewrite(t, insql, outsql, "An Error!", "Expected Rewrite to REJECT the query but it parsed ok")
	}
}

var schema *Schema

func init() {
	schema = NewSchema()
	if v, err := schema.Add("user", `SELECT id, name FROM member`); err != nil {
		panic(err)
	} else {
		if err := v.Grant(Update|Insert|Delete, "name"); err != nil {
			panic(err)
		}
	}
	if v, err := schema.Add("document", `SELECT id, member_id, name FROM private.document WHERE member_id IN (SELECT id FROM user)`); err != nil {
		panic(err)
	} else {
		if err := v.Grant(Update|Insert, "name", "member_id"); err != nil {
			panic(err)
		}
	}
	if v, err := schema.Add("old_members", `SELECT id, name, age FROM member WHERE age > $1`, 80); err != nil {
		panic(err)
	} else {
		if err := v.Grant(Update|Insert, "name", "age"); err != nil {
			panic(err)
		}
	}
	if v, err := schema.Add("really_old_members", `SELECT id, name, age FROM old_members WHERE age > $1`, 100); err != nil {
		panic(err)
	} else {
		if err := v.Grant(Update|Insert, "name", "age"); err != nil {
			panic(err)
		}
	}
}

func TestRewriteSelect(t *testing.T) {
	checkRewrite(t, schema,
		`SELECT id FROM user`,
		`WITH user AS (SELECT id, name FROM member) SELECT id FROM user`)
}

func TestRewriteUpdate(t *testing.T) {
	checkRewrite(t, schema,
		`UPDATE user SET name = $1`,
		`UPDATE member SET name = $1`)
	checkDenyRewrite(t, schema,
		`UPDATE user SET id = $1`)
	checkDenyRewrite(t, schema,
		`UPDATE member SET name = $1`)
	checkDenyRewrite(t, schema,
		`UPDATE member SET id = $1`)
}

func TestRewriteInsert(t *testing.T) {
	checkRewrite(t, schema,
		`INSERT INTO user (name) VALUES ($1)`,
		`INSERT INTO member (name) VALUES ($1)`)
	checkDenyRewrite(t, schema,
		`INSERT INTO user (id) VALUES ($1)`)
	checkDenyRewrite(t, schema,
		`INSERT INTO user (id, secret) VALUES ($1, $2)`)
	checkDenyRewrite(t, schema,
		`INSERT INTO member (name) VALUES ($1)`)
	checkDenyRewrite(t, schema,
		`INSERT INTO member (id) VALUES ($1)`)
}

func TestRewriteDelete(t *testing.T) {
	checkRewrite(t, schema,
		`DELETE FROM user`,
		`DELETE FROM member`)
	checkRewrite(t, schema,
		`DELETE FROM user WHERE id = $1`,
		`DELETE FROM member WHERE id = $1`)
	checkDenyRewrite(t, schema,
		`DELETE FROM member`)
}

func TestRewriteCteSelect(t *testing.T) {
	checkRewrite(t, schema,
		`SELECT id,name FROM document`,
		`WITH user AS (SELECT id, name FROM member), document AS (SELECT id, member_id, name FROM private.document WHERE member_id IN (SELECT id FROM user)) SELECT id, name FROM document`)
}

func TestRewriteInsertWithSelect(t *testing.T) {
	checkRewrite(t, schema,
		`INSERT INTO user (name) (SELECT name FROM user)`,
		`WITH user AS (SELECT id, name FROM member) INSERT INTO member (name) VALUES ((SELECT name FROM user))`)
	checkRewrite(t, schema,
		`INSERT INTO user (name) SELECT name FROM user`,
		`WITH user AS (SELECT id, name FROM member) INSERT INTO member (name) SELECT name FROM user`)
	checkRewrite(t, schema,
		`INSERT INTO user (name) VALUES ((SELECT name FROM user LIMIT 1))`,
		`WITH user AS (SELECT id, name FROM member) INSERT INTO member (name) VALUES ((SELECT name FROM user LIMIT 1))`)
}

func TestConsolidateWithStatement(t *testing.T) {
	checkRewrite(t, schema,
		`WITH custom_user AS (SELECT id AS pk FROM user) SELECT pk FROM custom_user`,
		`WITH user AS (SELECT id, name FROM member), custom_user AS (SELECT id AS pk FROM user) SELECT pk FROM custom_user`)
}

func TestShiftBindings(t *testing.T) {
	insql := `SELECT name FROM really_old_members WHERE age > $1 AND age < $2`
	expsql := `WITH old_members AS (SELECT id, name, age FROM member WHERE age > $1), really_old_members AS (SELECT id, name, age FROM old_members WHERE age > $2) SELECT name FROM really_old_members WHERE age > $3 AND age < $4`
	_, args := checkRewrite(t, schema, insql, expsql, 200, 300)
	if len(args) != 4 {
		t.Fatalf("Expected 4 args returned from the rewrite got %d (first shift)", len(args))
	}
	if args[0] != 80 {
		t.Fatalf("Expected arg[0] to be 100 got %d", args[0])
	}
	if args[1] != 100 {
		t.Fatalf("Expected arg[1] to be 100 got %d", args[1])
	}
	if args[2] != 200 {
		t.Fatalf("Expected arg[2] to be 200 got %d", args[2])
	}
	if args[3] != 300 {
		t.Fatalf("Expected arg[3] to be 300 got %d", args[3])
	}
	// Running this test twice tests that the binding shifting has had no side effects
	_, args = checkRewrite(t, schema, insql, expsql, 200, 300)
	if len(args) != 4 {
		t.Fatalf("It looks like the binding shift may have had side effects!")
	}

}
