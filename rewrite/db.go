package rewrite

import (
	"database/sql"
	"database/sql/driver"
	"fmt"
	realpq "github.com/lib/pq"
)

// Each time Open is called to configure a new RewriteConnection
// a new sql driver is registered. Yep, bit of a hack. This var
// keeps track of the drivers.
var rewriteDriverCount = 0

// A fake sql driver that wraps lib/pq
type drv struct {
	rw Rewriter
}

// Returns connction for sql package to use.
func (d *drv) Open(name string) (driver.Conn, error) {
	cn, err := realpq.Open(name)
	return &conn{cn: cn, rw: d.rw}, err
}

// Open a DB connction that will always apply the Rewriter rw to any queries
// that are made.
func Open(name string, rw Rewriter) (db *sql.DB, err error) {
	driverName := fmt.Sprintf("postgres_rw%d", rewriteDriverCount)
	sql.Register(driverName, &drv{rw: rw})
	db, err = sql.Open(driverName, name)
	if err != nil {
		return nil, err
	}
	rewriteDriverCount++
	return
}

// A fake conn that proxys to the underlying connection.
type conn struct {
	rw Rewriter
	cn driver.Conn
}

func (cn *conn) Begin() (_ driver.Tx, err error) {
	return cn.cn.Begin()
}

func (cn *conn) Commit() (err error) {
	return cn.cn.(driver.Tx).Commit()
}

func (cn *conn) Rollback() (err error) {
	return cn.cn.(driver.Tx).Rollback()
}

func (cn *conn) Close() (err error) {
	return cn.cn.Close()
}

func (cn *conn) Prepare(q string) (driver.Stmt, error) {
	q, args, err := cn.rw.Rewrite(q)
	if err != nil {
		return nil, err
	}
	if len(args) > 0 {
		return nil, fmt.Errorf("Prepare cannot be used to rewrite queries with binding arguments")
	}
	return cn.cn.Prepare(q)
}

func (cn *conn) Query(q string, args []driver.Value) (driver.Rows, error) {
	q, outargs, err := cn.rw.Rewrite(q, valsToInfs(args)...)
	if err != nil {
		return nil, err
	}
	return cn.cn.(driver.Queryer).Query(q, infsToVals(outargs))
}

func (cn *conn) Exec(q string, args []driver.Value) (driver.Result, error) {
	q, outargs, err := cn.rw.Rewrite(q, valsToInfs(args)...)
	if err != nil {
		return nil, err
	}
	return cn.cn.(driver.Execer).Exec(q, infsToVals(outargs))
}

func infsToVals(infs []interface{}) (vals []driver.Value) {
	vals = make([]driver.Value, len(infs))
	for i, _ := range infs {
		vals[i] = infs[i]
	}
	return vals
}

func valsToInfs(vals []driver.Value) (infs []interface{}) {
	vals = make([]driver.Value, len(vals))
	for i, _ := range vals {
		infs[i] = vals[i]
	}
	return infs
}
