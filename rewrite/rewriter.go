package rewrite

import (
	"bitbucket.org/pkg/pgutil/parser"
	"fmt"
	"strconv"
)

type Rewriter interface {
	Rewrite(insql string, inargs ...interface{}) (outsql string, outargs []interface{}, err error)
}

// The RewriterFunc type is an adapter to allow the use of ordinary functions as Rewriter.
// If f is a function with the appropriate signature, RewriterFunc(f) is a Handler object that calls f.
type RewriterFunc func(sql string, arg ...interface{}) (string, []interface{}, error)

func (f RewriterFunc) Rewrite(sql string, args ...interface{}) (string, []interface{}, error) {
	return f(sql, args)
}

const updateColumnName = "updated_" // column name added in RETURNING clauses

// Add parser.WithSource to e
func prependCTE(e *parser.Expr, source *parser.Expr) *parser.Expr {
	if !e.Is(parser.With) {
		with := parser.NewExpr(parser.With)
		e.Tag(parser.WithQuery)
		with.Append(e)
		e = with
	}
	e.Prepend(source)
	return e
}

// Increment any binding numbers in e by n
func shiftBindings(e *parser.Expr, n int) {
	for _, e := range e.Find(parser.Binding, true) {
		v, err := strconv.Atoi(e.GetValue())
		if err != nil {
			panic("Invalid binding token")
		}
		e.SetValue(strconv.FormatInt(int64(v+n), 10))
	}
}

// Add a default RETURNING statement if missing
func addReturning(e *parser.Expr) {
	if !e.Has(parser.Returning) {
		returning := parser.NewExpr(parser.Returning)
		returning.SetValue(fmt.Sprintf("1 AS %s", updateColumnName))
		e.Append(returning)
	}
}
