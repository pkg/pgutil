package rewrite

import (
	"bitbucket.org/pkg/pgutil/parser"
	"fmt"
)

const (
	tempTableName = "q_" // the temporary table name used for json wrapping
)

// A Rewriter that converts the query response into a single JSON
// row/column. If the query is an UPDATE/INSERT/DELETE then it will
// just return a count of the number of affected rows
var JSON *jsonRewriter = &jsonRewriter{}

type jsonRewriter struct{}

var jsonAggQuery string = fmt.Sprintf(`SELECT json_agg(%[1]s) FROM %[1]s`, tempTableName)
var jsonReturningQuery string = fmt.Sprintf(`SELECT '{"count":' || CAST(count(%[1]s) AS text) || '}' FROM %[2]s`, updateColumnName, tempTableName)

func (rw *jsonRewriter) Rewrite(q string, args ...interface{}) (_ string, _ []interface{}, err error) {
	e, err := parser.Parse(q)
	if err != nil {
		return
	}
	jsonify := jsonAggQuery

	// When wrapping an update, insert or delete we want to be
	// able to fetch a count of affected rows, so we add a RETURNING if it's missing
	isUpdate := false
	if e.Is(parser.Update) || e.Is(parser.Insert) || e.Is(parser.Delete) {
		isUpdate = true
		addReturning(e)
	}
	for _, e := range e.Find(parser.Update, true) {
		isUpdate = true
		addReturning(e)
	}
	for _, e := range e.Find(parser.Insert, true) {
		isUpdate = true
		addReturning(e)
	}
	for _, e := range e.Find(parser.Delete, true) {
		isUpdate = true
		addReturning(e)
	}
	if isUpdate {
		jsonify = jsonReturningQuery
	}

	stmt, err := parser.Parse(jsonify)
	if err != nil {
		return
	}

	// We need to wrap the query in a WITH
	source := parser.NewExpr(parser.WithSource)
	source.SetValue(tempTableName)
	if e.Is(parser.With) {
		query, err := e.Get(parser.WithQuery)
		if err != nil {
			return "", nil, err
		}
		query.Tag(parser.Source)
		source.Append(query)
		e.Append(source)
		stmt.Tag(parser.WithQuery)
		e.Replace(parser.WithQuery, stmt)
	} else {
		e.Tag(parser.Source)
		source.Append(e)
		e = parser.NewExpr(parser.With)
		e.Append(source)
		stmt.Tag(parser.WithQuery)
		e.Append(stmt)
	}
	return e.String(), args, nil
}
