package rewrite

import (
	"fmt"
	"testing"
)

// These are end to end tests.
// You must have a postgres database (pqutil_test) setup and listening
// You can use the PGUSER etc environment variables to configure.
// See test.sh for bootstrapping.

func TestDB(t *testing.T) {
	exp := `OK`
	rw := RewriterFunc(func(q string, args ...interface{}) (string, []interface{}, error) {
		return fmt.Sprintf("SELECT '%s'", exp), nil, nil
	})
	db, err := Open("dbname=pqutil_test sslmode=disable", rw)
	if err != nil {
		panic(err)
	}
	var q = `SELECT ANYTHING AS THIS WILL GET REWRITTEN`
	var args = make([]interface{}, 0)
	var out string
	err = db.QueryRow(q, args...).Scan(&out)
	if err != nil {
		t.Fatal(err)
	}
	if out != exp {
		t.Fatalf("Response did not match.\nGot     : %sExpected: %s", out, exp)
	}
}
