package rewrite

import (
	"fmt"
	"testing"
)

func checkWrapJson(t *testing.T, insql string, returning bool) {
	outsql, _, err := JSON.Rewrite(insql)
	ret := ""
	jsonify := jsonAggQuery
	if returning {
		ret = fmt.Sprintf(" RETURNING 1 AS %s", updateColumnName)
		jsonify = jsonReturningQuery
	}
	expsql := fmt.Sprintf(`WITH %[3]s AS (%[1]s%[2]s) %[4]s`, insql, ret, tempTableName, jsonify)
	if err != nil {
		fatalRewrite(t, insql, outsql, expsql, err.Error())
	}
	if insql == outsql {
		fatalRewrite(t, insql, outsql, expsql, "Did not rewrite query at all!")
	}
	if outsql != expsql {
		fatalRewrite(t, insql, outsql, expsql)
	}
}

func TestWrapJsonSelect(t *testing.T) {
	checkWrapJson(t, `SELECT id FROM user WHERE x = $1`, false)
}

func TestWrapJsonUpdate(t *testing.T) {
	checkWrapJson(t, `UPDATE user SET x = $1`, true)
}

func TestWrapJsonInsert(t *testing.T) {
	checkWrapJson(t, `INSERT INTO user (x) VALUES ($1)`, true)
}

func TestWrapJsonDelete(t *testing.T) {
	checkWrapJson(t, `DELETE FROM user WHERE x = $1`, true)
}
