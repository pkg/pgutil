var Flux = require('flux');
var Item = require('./Item');
var TodoStore = require('../stores/TodoStore');
var DOM = Flux.DOM;

var Update = TodoStore.Update;

var Types = Flux.Types;

module.exports = Flux.Component({

  propTypes: {
    todos: Types.object.isRequired
  },

  allComplete: function(){
    return !this.props.todos.some('complete');
  },

  toggleAll: function(){
    Update(this.props.todos, function(todo){
      this.comleted = !this.completed;
    });
  },

  render: function() {

    // This section should be hidden by default
    // and shown when there are todos.
    if( this.props.todos.length < 1 ){
      return null;
    }

    return DOM.section({id: "main"}, 
      DOM.input({
        id: "toggle-all",
        type: "checkbox",
        onChange: this.toggleAll,
        checked: this.allComplete() ? 'checked' : ''
      }),
      DOM.label({htmlFor: "toggle-all"}, "Mark all as complete"),
      DOM.ul({id: "todo-list"}, this.props.todos.mapValues(function(todo){
        return Item({todo: todo});
      }))
    );

  }

});
