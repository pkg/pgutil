var Flux = require('flux');
var Types = Flux.Types;
var DOM = Flux.DOM;

var ENTER_KEY_CODE = 13;

module.exports = Flux.Component({

  propTypes: {
    className:   Types.string,
    id:          Types.string,
    placeholder: Types.string,
    onSave:      Types.func.isRequired,
    value:       Types.string
  },

  getInitialState: function() {
    return {
      value: this.props.value || ''
    };
  },

  onChange: function(event) {
    this.setState({
      value: event.target.value
    });
  },

  onKeyDown: function(event) {
    if (event.keyCode === ENTER_KEY_CODE) {
      this.save();
    }
  },

  save: function(){
    if( !this.state.value ){
      return;
    }
    this.props.onSave(this.state.value);
    this.setState({
      value: ''
    });
  },

  render: function(){
    return DOM.input({
      className: this.props.className,
      id: this.props.id,
      placeholder: this.props.placeholder,
      onBlur: this.save,
      onChange: this.onChange,
      onKeyDown: this.onKeyDown,
      value: this.state.value,
      autoFocus: true
    });
  }

});
