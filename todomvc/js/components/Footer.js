var Flux = require('flux');
var Destroy = require('../stores/TodoStore').Destroy;
var DOM = Flux.DOM;

var Types = Flux.Types;

module.exports = Flux.Component({

  propTypes: {
    todos: Types.object.isRequired
  },

  destroyCompleted: function(){
    Destroy(this.props.todos.filter(function(todo){
      return todo.completed;
    }))
  },

  completeCount: function(){
    return this.props.todos.reduce(function(count, todo){
      count += todo.completed ? 1 : 0
    },0)
  },

  remainingCount: function(){
    return this.props.todos.size() - this.completeCount();
  },

  renderClearButton: function(){
    var completed = this.completeCount();
    if( !completed ){
      return null;
    }
    return DOM.button({id: "clear-completed", onClick: this.destroyCompleted}, "Clear completed (", completed, ")");
  },

  renderStatus: function(){
    var remaining = this.remainingCount();
    return DOM.span({id: "todo-count"}, DOM.strong(remaining), remaining === 1 ? ' item ' : ' items ', 'remaining')
  },

  render: function() {
    if( this.props.todos.size() == 0 ){
      return null;
    }
  	return DOM.footer({id: "footer"},
      this.renderStatus(),
      this.renderClearButton()
    );
  }

});

