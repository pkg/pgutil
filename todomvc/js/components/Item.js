var Flux = require('flux');
var TodoStore = require('../stores/TodoStore');
var TextInput = require('./TextInput');
var Update = TodoStore.Update;
var Destroy = TodoStore.Destroy;
var Types = Flux.Types;
var DOM = Flux.DOM;

module.exports = Flux.Component({

  propTypes: {
    todo: Types.object.isRequired
  },

  getInitialState: function() {
    return {
      isEditing: false
    };
  },

  edit: function() {
    this.setState({isEditing: true});
  },

  updateText: function(text) {
    Update(this.props.todo, function(){
      this.text = text;
    });
    this.setState({isEditing: false});
  },

  destroy: function(){
    Destroy(this.props.todo);
  },

  toggle: function(){
    Update(this.props.todo, function(){
      this.complete = !this.complete;
    });
  },

  classNames: function(){
    return Flux.classSet({
      completed: this.props.todo.complete,
      editing: this.state.isEditing
    });
  },

  renderEditBox: function(){
    if( !this.state.isEditing ){
      return null;
    }
    return TextInput({
      className: "edit",
      onSave: this.updateText,
      value: this.props.todo.text
    });
  },

  render: function() {
    var todo = this.props.todo;
    return DOM.li({className: this.classNames(), key: todo.id},
      DOM.div({className: "view"},
        DOM.input({className: "toggle", type: "checkbox", checked: todo.complete, onChange: this.toggle}),
        DOM.label({onDoubleClick: this.edit}, todo.text),
        DOM.button({className: "destroy", onClick: this.destroy})
      ),
      this.renderEditBox()
    );
  }

});
