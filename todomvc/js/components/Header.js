var Flux = require('flux');
var TextInput = require('./TextInput');
var TodoStore = require('../stores/TodoStore');
var Create = TodoStore.Create;
var DOM = Flux.DOM;

module.exports = Flux.Component({

  create: function(text) {
    if (text.trim()){
      Create({text: text});
    }
  },

  render: function() {
    return DOM.header({id: "header"},
      DOM.h1({}, "todos"),
      TextInput({
        id: "new-todo",
        placeholder: "What needs to be done?",
        onSave: this.create,
      })
    );
  }

});
