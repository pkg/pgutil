
var Flux = require('flux');
var Footer = require('./components/Footer');
var Header = require('./components/Header');
var List = require('./components/List');
var TodoStore = require('./stores/TodoStore');
var DOM = Flux.DOM;

module.exports = Flux.App({

  init: function(){
    this.assign(TodoStore, 'todos');
  },

  render: function() {
  	return DOM.section({id: "todoapp"},
  		DOM.div({},
			Header(),
			List({todos: this.state.todos}),
			Footer({todos: this.state.todos})
		)
    );
  }

});

TodoStore.onValue(function(v){
	console.log('Store state', v)
});

Flux.ActionBus.onValue(function(v){
	console.log('ActionBus', v);
});

TodoStore.onError(function(e){
	console.error(e);
});

Flux.render();
