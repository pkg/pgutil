package main

import (
	"bitbucket.org/pkg/pgutil/httpq"
	"bitbucket.org/pkg/pgutil/rewrite"
	"database/sql"
	_ "github.com/lib/pq"
	"log"
	"net/http"
)

// Worlds worst authentication system
var users = map[string]int{
	"1234-AAAA": 1,
	"1234-BBBB": 2,
}

// A function that builds a
func views(r *http.Request) (rewrite.Rewriter, error) {
	// authenticate the user by looking for an X-Token header
	key := r.Header.Get("X-Token")
	user, ok := users[key]
	if !ok {
		return nil, httpq.Forbidden
	}
	// build a view of the schema specificly for the user
	schema := rewrite.NewSchema()

	v, _ := schema.Add("member", `
		SELECT id,name 
		FROM private.member 
		WHERE id IN (SELECT member_id FROM organisation_member)`)
	v.Grant(rewrite.Update, "name")

	v, _ = schema.Add("organisation_member", `
		SELECT member_id, organisation_id
		FROM private.organisation_member
		WHERE organisation_id IN (SELECT id FROM organisation)`)

	v, _ = schema.Add("organisation", `
		SELECT id, name 
		FROM private.organisation 
		WHERE id IN (
		 	SELECT id 
		 	FROM private.organisation, private.organisation_member 
		 	WHERE organisation_id = organisation.id AND member_id = $1)`, user)
	v.Grant(rewrite.Update, "organisation_id", "name")

	v, _ = schema.Add("document", `
		SELECT id, organisation_id, name 
		FROM private.document 
		WHERE organisation_id IN (SELECT id FROM organisation)`)
	v.Grant(rewrite.Update, "organisation_id", "name")

	return schema, nil
}

func main() {
	// connect to the database
	db, err := sql.Open("postgres", "dbname=pqutil_test sslmode=disable")
	if err != nil {
		panic(err)
	}
	h := httpq.NewHandler(db, views)
	api := h.(*httpq.RewriteHandler)
	api.Debug = true
	http.Handle("/pq", h)
	http.Handle("/", http.FileServer(http.Dir("./")))
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", nil))
}
