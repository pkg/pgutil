var Promise = require('es6-promise').Promise;
var Client = require('../httpqc').Client;


var pq = new Client({
	authenticate: function(){
		return new Promise(function(resolve, reject){
			var key = prompt("Enter API key (hint: 1234-AAAA)");
			if(!key){
				return reject(Error("Failed to get auth credentials"));	
			}
			resolve(key);
		});
	}
});

global.pq = pq;

//////////////////

var Flux = require('../flux');
var DOM = Flux.DOM;
var megabus = Flux.enableDebug();

var SHAKE = Flux.Action();

var RATE = Flux.Action();

var PeopleStream = Flux.Data({ // emit a random "person" every x seconds
	rate: RATE.scan(500, function(delay, t){
		delay += t;
		if( delay < 25 ){
			delay = 25;
		}
		if( delay > 1000 ){
			delay = 1000;
		}
		return delay;
	}),
	init: function(){
		this.assign(this.rate, 'ms');
		this.emit();
	},
	emit: function(){
		this.push({
			id: parseInt(Math.random()*100,10),
			name: 'name-' + parseInt(Math.random()*100000),
			color: ['orange', 'yellow', 'white'][parseInt(Math.random()*3,10)]
		})
		setTimeout(this.emit.bind(this), this.ms);
	}
})

// Take a stream of people, and turn it into a property hash of people keyed by id
var PeopleData = Flux.Store({
	init: function(){
		this.on(SHAKE, this.shake);
		this.plug(PeopleStream);
	},
	shake: function(name){
		alert('shake that ass, '+name);
	}
});

// Render a person
var Item = Flux.Component({
	render: function(){
		return DOM.div({className: 'block'}, 
			DOM.a({
				style: {color: this.props.color},
				onClick: function(){
					SHAKE.push(this.props.name)
				}.bind(this)}, 
				this.props.name
			)
		)
	}
});

// Render the app
var app = Flux.App({
	init: function(){
		this.assign(PeopleData, 'people');
		this.assign(PeopleStream.rate, 'rate');
	},
	render: function(){
		return DOM.div({},

			DOM.button({className:'control', onClick: function(){
				RATE.push(-25);
			}}, 'FASTER'),

			DOM.button({className: 'control', onClick: function(){
				RATE.push(+25);
			}}, 'SLOWER'),

			DOM.div({className: 'rate'}, 'refreshing every ', this.state.rate, 'ms'),

			this.state.people.mapValues(Item)

		);
	}
});

Flux.render();
