package parser

import "testing"

func shouldParse(t *testing.T, input string) {
	stmt, err := Parse(input)
	if err != nil {
		t.Fatal(err)
	}
	output := stmt.String()
	if input != output {
		t.Fatalf("Input did not match generated output\nInput : %s\nOutput: %s", input, output)
	}
}

func shouldFail(t *testing.T, input string) {
	_, err := Parse(input)
	if err == nil {
		t.Fatalf("The given input SHOULD have failed but it parsed ok\nInput: %s", input)
	}
}

func TestParserPop(t *testing.T) {
	p := newParser("")
	p.push(NewExpr(Value))
	p.push(NewExpr(Literal))
	p.push(NewExpr(Binding))
	if len(p.stack) != 3 {
		t.Fatalf("Stack should have been 3 got %d", len(p.stack))
	}
	e := p.pop(Binding)
	if !e.Is(Binding) {
		t.Fatalf("Expected poped expr to be type %v got %v", Binding, e)
	}
	if len(p.stack) != 2 {
		t.Fatalf("Stack should have been 2 got %d", len(p.stack))
	}
}

func TestParserPopPanic(t *testing.T) {
	var e *Expr
	defer func() {
		if err := recover(); err == nil {
			t.Fatalf("Expected parser to panic when popping invalid element but got %v", e)
		}
	}()
	p := newParser("")
	p.push(NewExpr(Value))
	p.push(NewExpr(Literal))
	e = p.pop(Value)
}

func TestParserTag(t *testing.T) {
	p := newParser("")
	p.push(NewExpr(Value))
	p.tag(Value, Literal)
	e := p.pop(Literal)
	if len(p.stack) != 0 {
		t.Fatalf("expected to pop off newly tagged item")
	}
	if !e.Is(Value) && !e.Is(Literal) {
		t.Fatalf("expected exp to be tagged as both Value and Literal")
	}
}

func TestParserPopN(t *testing.T) {
	p := newParser("")
	p.push(NewExpr(Statement))
	p.push(NewExpr(Value))
	p.push(NewExpr(Value))
	p.push(NewExpr(Value))
	p.push(NewExpr(Expression))
	if len(p.stack) != 5 {
		t.Fatalf("Stack should have been 5 got %d", len(p.stack))
	}
	es := p.popN(Statement, 0, 1)
	if len(es) > 0 {
		t.Fatalf("Should have ignored optional pop but got %v", es)
	}
	if len(p.stack) != 5 {
		t.Fatalf("Stack should have remained 5 but got %d", len(p.stack))
	}
	es = p.popN(Expression, 0, -1)
	if len(es) != 1 {
		t.Fatalf("Expected one Expression pop got %v", es)
	}
	if len(p.stack) != 4 {
		t.Fatalf("Stack should now be 5 but got %d", len(p.stack))
	}
	es = p.popN(Value, 1, 1)
	if len(es) != 1 {
		t.Fatalf("Expected one Value pop got %v", es)
	}
	if len(p.stack) != 3 {
		t.Fatalf("Stack should now be 3 but got %d", len(p.stack))
	}
	es = p.popN(Value, 0, -1)
	if len(es) != 2 {
		t.Fatalf("Expected two Value pop got %v", es)
	}
	if len(p.stack) != 1 {
		t.Fatalf("Stack should now be 3 but got %d", len(p.stack))
	}
}

func TestParserPopNPanic(t *testing.T) {
	var es []*Expr
	defer func() {
		if err := recover(); err == nil {
			t.Fatalf("Expected parser to panic when popN invalid element but got %v", es)
		}
	}()
	p := newParser("")
	p.push(NewExpr(Value))
	p.push(NewExpr(Literal))
	es = p.popN(Value, 2, -1)
}

func TestSelect(t *testing.T) {
	shouldParse(t, `SELECT a, b, c FROM t1 WHERE a = $1`)
	shouldParse(t, `SELECT a_b, b_b, c_c FROM t2 WHERE a = $1 AND b = $2 OR c = $3`)
	shouldParse(t, `SELECT a, b, c FROM t3 WHERE a = $1 AND (b = $2 OR b = $3)`)
}

func TestSelectOrder(t *testing.T) {
	shouldParse(t, `SELECT a, b, c FROM t1 ORDER BY a`)
	shouldParse(t, `SELECT a_b, b_b, c_c FROM t2 ORDER BY c WHERE a = $1 AND b = $2 OR c = $3`)
	shouldParse(t, `SELECT a, b, c FROM t3 WHERE a = $1 AND (b = $2 OR b = $3) ORDER BY b, c`)
	shouldParse(t, `SELECT a, b, c FROM t4 ORDER BY a DESC, b ASC`)
}

func TestSelectLimit(t *testing.T) {
	shouldParse(t, `SELECT a, b, c FROM t1 LIMIT 5`)
	shouldParse(t, `SELECT a, b, c FROM t1 LIMIT 5`)
}

func TestSelectOffset(t *testing.T) {
	shouldParse(t, `SELECT a, b, c FROM t1 OFFSET 1`)
	shouldParse(t, `SELECT a, b, c FROM t2 LIMIT 5 OFFSET 10`)
}

func TestSelectGroup(t *testing.T) {
	shouldParse(t, `SELECT a, b, c FROM t1 GROUP BY a`)
	shouldParse(t, `SELECT a, b, array_agg(c) FROM t2 GROUP BY a, b`)
}

func TestSelectHaving(t *testing.T) {
	shouldParse(t, `SELECT a, b, c FROM t1 GROUP BY a HAVING a > 100`)
	shouldParse(t, `SELECT a, b, array_agg(c) FROM t2 GROUP BY a, b HAVING a > 100 OR b = true`)
}

func TestSelectAllFilters(t *testing.T) {
	shouldParse(t, `SELECT a, b, array_agg(c) FROM t2 WHERE a != 35 GROUP BY a, b HAVING a > 100 OR b = true LIMIT 10 OFFSET 200`)
}

func TestUpdate(t *testing.T) {
	shouldParse(t, `UPDATE t1 SET a = $1, b = $2`)
	shouldParse(t, `UPDATE t2 SET a = $1, b = $2 WHERE z = $3`)
	shouldParse(t, `UPDATE t3 SET a = $1, b = $2 WHERE x = $3 AND (z = $4 OR z = $5)`)
}

func TestInsert(t *testing.T) {
	shouldParse(t, `INSERT INTO t1 (a) VALUES ($1)`)
	shouldParse(t, `INSERT INTO t2 (a, b) VALUES ($1)`)
	shouldParse(t, `INSERT INTO t3 (a, b, c) VALUES ($1, $2, $3)`)
}

func TestDelete(t *testing.T) {
	shouldParse(t, `DELETE FROM t1`)
	shouldParse(t, `DELETE FROM t2 WHERE a = $1`)
	shouldParse(t, `DELETE FROM t3 WHERE (b = $2 OR b = $3) AND a = $1`)
}

func TestSelectNumeric(t *testing.T) {
	shouldParse(t, `SELECT 1, 999, 300234`)
}

func TestSelectFunctions(t *testing.T) {
	shouldParse(t, `SELECT count(id)`)
	shouldParse(t, `SELECT count(id) AS x`)
	shouldParse(t, `SELECT MIN(a), max(b), cos($1)`)
	shouldParse(t, `SELECT row_to_json(id)`)
	shouldParse(t, `SELECT row_to_json(ROW(id, name))`)
}

func TestSelectString(t *testing.T) {
	shouldParse(t, `SELECT '1', 'Abc123', 'Jeff Jefferson'`)
	shouldFail(t, `SELECT '\''`)
	shouldFail(t, `SELECT '\000'`)
	shouldFail(t, `SELECT 'é'`)
}

func TestIsNullOp(t *testing.T) {
	shouldParse(t, `SELECT a FROM t1 WHERE a IS NULL`)
}

func TestIsNotNullOp(t *testing.T) {
	shouldParse(t, `SELECT a FROM t1 WHERE a IS NOT NULL`)
}

func TestSelectColumns(t *testing.T) {
	shouldParse(t, `SELECT A, b, c FROM t1`)
	shouldFail(t, `SELECT 02sss FROM t2`)
	shouldFail(t, `SELECT "Abc" FROM t3`)
}

func TestSelectQualifiedColumns(t *testing.T) {
	shouldParse(t, `SELECT t1.a, t2.b FROM t1, t2`)
}

func TestSelectAliasedColumns(t *testing.T) {
	shouldParse(t, `SELECT a AS b, c AS D FROM table_name`)
	shouldParse(t, `SELECT 1 AS one, ('2') AS TWO, three AS four FROM table_name`)
}

func TestSelectFromSource(t *testing.T) {
	shouldParse(t, `SELECT a, b, c FROM t1`)
	shouldParse(t, `SELECT a, b, c FROM t1, t2, t3`)
	shouldParse(t, `SELECT a, b, c FROM t1 AS teeone, t2 AS teetwo, t3`)
}

func TestSelectWithLiterals(t *testing.T) {
	shouldParse(t, `SELECT a, b, c FROM t1 WHERE a = 1`)
	shouldParse(t, `SELECT a, b, c FROM t2 WHERE a = 1 AND b = '2'`)
	shouldParse(t, `SELECT a, b, c FROM t3 WHERE a = 1 AND (b = 2 OR b = 3)`)
}

func TestWithStatement(t *testing.T) {
	shouldParse(t, `WITH x AS (SELECT a, b, c FROM t1) SELECT a FROM x`)
}

func TestCast(t *testing.T) {
	shouldParse(t, `SELECT CAST(1 AS int)`)
	shouldParse(t, `SELECT CAST(a AS text)`)
	shouldFail(t, `SELECT a::int`)
}

// func TestSelectArray(t *testing.T) {
// 	shouldParse(t, `SELECT ARRAY[1,2,3]`)
// }
