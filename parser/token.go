package parser

import (
	"fmt"
	"strings"
)

type T uint64

// Dump out all the flags as strings.
// Basically just for debugging.
func (t T) String() string {
	ss := make([]string, 0)
	for f := T(1); f != 0; f = f << 1 {
		if !t.is(f) {
			continue
		}
		s := ""
		switch f {
		case Value:
			s = "Value"
		case Literal:
			s = "Literal"
		case Binding:
			s = "Binding"
		case Column:
			s = "Column"
		case Alias:
			s = "Alias"
		case Ident:
			s = "Ident"
		case Table:
			s = "Table"
		case Cast:
			s = "Cast"
		case Case:
			s = "Case"
		case Else:
			s = "Else"
		case When:
			s = "When"
		case Function:
			s = "Function"
		case FunctionName:
			s = "FunctionName"
		case FunctionArg:
			s = "FunctionArgs"
		case ColumnName:
			s = "ColumnName"
		case Returning:
			s = "Returning"
		case Operator:
			s = "Operator"
		case Unary:
			s = "Unary"
		case Binary:
			s = "Binary"
		case Boolean:
			s = "Boolean"
		case Between:
			s = "Between"
		case Join:
			s = "Join"
		case Compound:
			s = "Compound"
		case Assignment:
			s = "Assignment"
		case Statement:
			s = "Statement"
		case Select:
			s = "Select"
		case Update:
			s = "Update"
		case Delete:
			s = "Delete"
		case Insert:
			s = "Insert"
		case With:
			s = "With"
		case WithQuery:
			s = "WithQuery"
		case Distinct:
			s = "Distinct"
		case Filter:
			s = "Filter"
		case Order:
			s = "Order"
		case OrderDir:
			s = "OrderDir"
		case Expression:
			s = "Expression"
		case Source:
			s = "Source"
		case WithSource:
			s = "WithSource"
		case QualifiedName:
			s = "QualifiedName"
		case Group:
			s = "Group"
		case Left:
			s = "Left"
		case Right:
			s = "Right"
		default:
			s = fmt.Sprintf("unknown (%d)", f)
		}
		ss = append(ss, s)
	}
	return strings.Join(ss, ",")
}

// Check if flag f is set on t
func (t T) is(f T) bool {
	return t&f != 0
}

const (
	Value T = 1 << iota // All Values
	Literal
	Binding
	Column
	Alias
	Ident
	Table
	Cast
	Case
	Else
	When
	Function
	FunctionName
	FunctionArg
	ColumnName
	Returning

	Operator // All ops
	Unary
	Binary
	Boolean
	Between
	Join
	Compound
	Assignment

	Statement // All statements
	Select
	Update
	Delete
	Insert
	With
	WithQuery
	Distinct

	Filter   // Matches WHERE, GROUP BY, HAVING etc.
	Order    // An ordering term
	OrderDir // DESC or ASC
	Expression
	Source        // A SELECT source
	WithSource    // An aliased SELECT source
	QualifiedName // A source schema or column's target

	Group // Groups exp in parens
	Left  // Used for ops
	Right // Used for ops

	Raw // Just raw SQL
	Any = ^T(0)
)

func Parse(sql string) (*Expr, error) {
	s := newParser(sql)
	s.Init()
	if err := s.Parse(); err != nil {
		return nil, err
	}
	// s.PrintSyntaxTree()
	s.Execute()
	return s.pop(Statement), nil
}

func newParser(sql string) *state {
	s := &state{Buffer: sql}
	s.stack = make([]*Expr, 0)
	return s
}

func (s *state) push(e *Expr) {
	s.stack = append(s.stack, e)
}

// debug
func (s *state) dump() {
	for i, e := range s.stack {
		fmt.Printf("%d: isFilter:%v isTable:%v isSource:%v isColumn:%v isExpression:%v isIdent:%v isColumnName:%v %s\n", i, e.Is(Filter), e.Is(Table), e.Is(Source), e.Is(Column), e.Is(Expression), e.Is(Ident), e.Is(ColumnName), e)
	}
}

// p.Tag(X, Y)
//
// ...is the equivilent to doing...
//
// e := p.pop(X)
// e.Tag(Y)
// p.push(e)
//
// but faster
func (s *state) tag(t T, tag T) {
	e := s.pop(t)
	e.Tag(tag)
	s.push(e)
}

// Pop at least <min> but no more than <max> contiguous expressions
// of type t from the stack.
// If there are not at least <min> expressions to pop it will panic.
func (s *state) popN(t T, min uint, max int) []*Expr {
	i := len(s.stack)
	if i == 0 {
		if min > 0 {
			panic("Stack is empty")
		}
		return nil
	}
	for i > 0 && (max == -1 || (len(s.stack)-i) < max) {
		i--
		if !s.stack[i].Is(t) {
			i++
			break
		}
	}
	es := s.stack[i:len(s.stack)]
	if uint(len(es)) < min {
		panic(fmt.Sprintf("Expected to pop at least %d expressions of type %v from the stack but got %d", min, t, len(es)))
	}
	s.stack = s.stack[0:i]
	return es
}

// Pop one expression of type t from the stack.
// If there is not an expression to pop it will panic.
func (s *state) pop(t T) *Expr {
	return s.popN(t, 1, 1)[0]
}

type Expr struct {
	T    T
	subs []*Expr
	v    string
}

func NewExpr(t T) *Expr {
	e := &Expr{}
	e.T = t
	e.subs = make([]*Expr, 0)
	return e
}

// Create a copy of this expression tree
func (e *Expr) Clone() *Expr {
	e2 := &Expr{}
	e2.T = e.T
	e2.v = e.v
	e2.subs = make([]*Expr, len(e.subs))
	for i := range e.subs {
		e2.subs[i] = e.subs[i].Clone()
	}
	return e2
}

// Add a sub expressions to this expression.
// Tag it with t on the way in.
func (e *Expr) add(t T, es ...*Expr) {
	for _, e := range es {
		e.Tag(t)
	}
	e.Append(es...)
}

func (e *Expr) Append(es ...*Expr) {
	e.subs = append(e.subs, es...)
}

func (e *Expr) Prepend(es ...*Expr) {
	e.subs = append(es, e.subs...)
}

// Same as get but more panicy
func (e *Expr) get(t T) *Expr {
	ex, err := e.Get(t)
	if err != nil {
		panic(err)
	}
	return ex
}

// Get the first exp for key
func (e *Expr) Get(t T) (ex *Expr, err error) {
	for _, e := range e.subs {
		if e.Is(t) {
			return e, nil
		}
	}
	return nil, fmt.Errorf("Could not find sub expression of type %v", t)
}

// Check if this exp is tagged t.
func (e *Expr) Is(t T) bool {
	return e.T.is(t)
}

// Tag this exp with tag t.
func (e *Expr) Tag(t T) T {
	e.T = e.T | t
	return e.T
}

// Check if there is a sub with tag t
func (e *Expr) Has(t T) bool {
	for _, e := range e.subs {
		if e.Is(t) {
			return true
		}
	}
	return false
}

// Find sub expressions tagged t.
// If bool is set it will recurse through ALL subs
func (e *Expr) Find(t T, deep bool) (es []*Expr) {
	es = make([]*Expr, 0)
	for _, e := range e.subs {
		if e.Is(t) {
			es = append(es, e)
		}
		if deep {
			es = append(es, e.Find(t, deep)...)
		}
	}
	return
}

// Swap the first sub expression that matches t with ex.
// Returns true if replaced false is not
func (e *Expr) Replace(t T, ex *Expr) bool {
	for i, sub := range e.subs {
		if sub.Is(t) {
			e.subs[i] = ex
			return true
		}
	}
	return false
}

// Return the value string
func (e *Expr) GetValue() string {
	return e.v
}

// Set the value data
func (e *Expr) SetValue(v string) {
	e.v = v
}

// Shortcut for joining string list of types tagged t.
func (e *Expr) Join(t T, joiner string) string {
	ss := make([]string, 0)
	for _, e := range e.Find(t, false) {
		ss = append(ss, e.String())
	}
	return strings.Join(ss, joiner)
}

// Build SQL string for the expression.
func (e *Expr) String() string {
	s := ""
	switch {
	case e.Is(Literal):
		s = e.GetValue()
	case e.Is(Operator):
		switch {
		case e.Is(Unary):
			s = e.GetValue() + e.get(Right).String()
		case e.Is(Join):
			a := e.get(Left).String()
			b := e.get(Right).String()
			switch e.GetValue() {
			case ",":
				s = a + ", " + b
			default:
				s = a + " " + e.GetValue() + " " + b
			}
			if e.Has(Filter) {
				s = s + " ON " + e.get(Filter).String()
			}
		case e.Is(Binary) || e.Is(Compound) || e.Is(Boolean):
			a := e.get(Left).String()
			b := e.get(Right).String()
			s = a + " " + e.GetValue() + " " + b
		case e.Is(Assignment):
			s = e.get(Column).String() + " = " + e.get(Value).String()
		case e.Is(Between):
			s = "BETWEEN"
		default:
			panic("Unknown operator")
		}
	case e.Is(Select):
		s = "SELECT "
		s = s + e.Join(Column, ", ")
		if e.Has(Source) {
			s = s + " FROM " + e.get(Source).String()
		}
		if e.Has(Filter) {
			s = s + " " + e.Join(Filter, " ")
		}
	case e.Is(Insert):
		s = "INSERT INTO " + e.get(Table).String() + " ("
		s = s + e.Join(Column, ", ")
		s = s + ") "
		switch {
		case e.Has(Select):
			s = s + e.get(Select).String()
		default:
			s = s + "VALUES (" + e.Join(Value, ", ") + ")"
		}
	case e.Is(Update):
		s = "UPDATE " + e.get(Table).String() + " SET "
		s = s + e.Join(Assignment, ", ")
		if e.Has(Filter) {
			s = s + " " + e.Join(Filter, " ")
		}
	case e.Is(Delete):
		s = "DELETE FROM " + e.get(Table).String()
		if e.Has(Filter) {
			s = s + " " + e.Join(Filter, " ")
		}
	case e.Is(With):
		s = "WITH " + e.Join(WithSource, ", ") + " " + e.get(WithQuery).String()
	case e.Is(WithSource):
		s = e.GetValue() + " AS (" + e.get(Source).String() + ")"
	case e.Is(Filter):
		s = e.GetValue() + " "
		switch {
		case e.Has(Order):
			s = s + e.Join(Order, ", ")
		case e.Has(Expression):
			s = s + e.get(Expression).String()
		default:
			panic("Invalid filter")
		}
	case e.Is(ColumnName):
		s = e.GetValue()
		if e.Has(QualifiedName) {
			s = e.get(QualifiedName).String() + "." + s
		}
	case e.Is(Table):
		s = e.GetValue()
		if e.Has(QualifiedName) {
			s = e.get(QualifiedName).String() + "." + s
		}
	case e.Is(Function):
		s = e.get(FunctionName).String()
		s = s + "(" + e.Join(FunctionArg, ", ") + ")"
	case e.Is(Binding):
		s = "$" + e.GetValue()
	case e.Is(Cast):
		s = "CAST(" + e.get(Expression).String() + " AS " + e.GetValue() + ")"
	case e.Is(Ident):
		s = e.GetValue()
	case e.Is(Group):
		s = "(" + e.get(Expression).String() + ")"
	case e.Is(Order):
		s = e.get(Expression).String()
		if e.Has(OrderDir) {
			s = s + " " + e.get(OrderDir).GetValue()
		}
	case e.Is(Raw):
		s = e.GetValue()
	default:
		panic(fmt.Sprintf("Do not know how to convert Expr of type '%s' to SQL", e.T))
	}
	if e.Has(Returning) {
		s = s + " RETURNING " + e.get(Returning).GetValue()
	}
	if e.Has(Alias) {
		s = s + " AS " + e.get(Alias).String()
	}
	return s
}

// Returns any source relations used in this expression
func (e *Expr) Sources() []string {
	sources := make([]string, 0)
	// if e.A != nil {
	// 	sources = append(sources, e.A.Sources()...)
	// }
	// if e.B != nil {
	// 	sources = append(sources, e.B.Sources()...)
	// }
	// if e.C != nil {
	// 	sources = append(sources, e.C.Sources()...)
	// }
	// if e.Stmt != nil {
	// 	sources = append(sources, e.Stmt.Sources()...)
	// }
	// if e.T == SourceExpr {
	// 	sources = append(sources, &Source{
	// 		Name:    e.Value,
	// 		Columns: e.Columns(),
	// 	})
	// }
	return sources
}
