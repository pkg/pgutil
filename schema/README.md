
schema
======

Converts a JSON representation of the tables/objects (in a format very similar to JSON-schema), to SQL DDL commands.

This is not a migration tool itself, it just generates the DDLs which can be simply run against postgres.

inputs
------

```javascript
Types = {
	uuid: {
		
	},
	enum: function(values, opts){
		
	},
	line: function(opts){
		if( opts.clean !== false ){
			// add basic clean validation
		}
		if( opts.multiline ){
			// allow linebreaks
		}
		return {type: "UUID NOT NULL", triggers: [
			function(){ clean },
			function(){ rejectMultiline },
			etc.
		]}
	},
	: function(){
		return Types.string({multi:true});
	}
};

{
	person: { // all objects have a UUID pk
		name: {
			type: Types.string()
		},
		group_ids: {
			type: Types.arrayOf(Types.idFor("person")), // Handles the reference constraint
		}
	},
	group: {
		name: {
			type: Types.string()
		}
	}
}
```

Outputs
-------

* Postgres DDL statements

Usage
-----

schemafy | psql mydb
