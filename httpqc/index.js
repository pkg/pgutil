var Promise = require('es6-promise').Promise;
var request = require('reqwest');
var URLSafeBase64 = require('urlsafe-base64');
var extend = require('extend');

/*
## `Client`

Instances of `Client` are used to communicate with httpq servers.

```
var Client = require('httpq').Client;

var pq = new Client({
	url: '//api.myservice.com/pq',
	authenticate: function(){
		// This function must return a promise that returns an API key.
		// The key will then be added to the headers with each query.
		return new Promise(function(resolve, reject) {
			// perform your login logic to request an API Key
			var apiKey = "2352-346346-12345-26346-23445556";
			if (apiKey) {
				resolve(apiKey);
			} else {
				reject(Error("Failed to get a valid API Key"));
			}
		});
	}
});

pq.query('SELECT * FROM person WHERE age > $1', [50]).then(function(results){
	// do something with results
})

```
*/
function Client(config){
	this.config = extend({}, {
		url: '/pq',            // default API url
		queryOptionDefaults: { // default options for `query` method
			cachable: false
		},
		apiKey: null,          // current apiKey
		retries: 10            // max numnber of times `authenticate` will be run before it's considered fatal
	}, config);
}

/*
### `query`

Perform an SQL query against an httpq server. Returns a promise for the results.

```
pq.query('SELECT * FROM person WHERE age > $1', [50], {
	cache: true,  // should this query allow HTTP caching. (default: false only valid for SELECT queries)
}).then(function(results){
	// do something with results
});
```
*/
Client.prototype.query = function(sql, values, opts){
	var client = this;
	return new Promise(function(resolve, reject){
		if( !client.config.url ){
			reject(Error('no url defined for the httpq API'));
			return;
		}
		var o = extend({attempt: 0},
			client.config.queryOptionDefaults, 
			opts || {}
		);
		// Get the API key.
		client.authenticate().then(function(key){
			var r = {
				url: client.config.url,
				type: 'json',
				method: 'post',
				contentType: 'application/json',
				headers: {'X-Token': key},
				data: JSON.stringify({query: sql, values: values})
			};
			// If query is a SELECT and cachable send in querystring
			if( o.cachable === true ){
				r.method = 'get';
				r.data = {q: URLSafeBase64.encode(r.data)};
			}
			// Make the request.
			return request(r)
		}).then(resolve).catch(function(err){
			var msg;
			o.attempt += 1;
			// Clear api key if invalid.
			if( err.status === 403 ){
				client.config.apiKey = null;
				// Rerun the authenticate func to get a new key, then retry the query.
				if( o.attempt < client.config.retries ){
					client.query(sql, values, o).then(resolve, reject);
					return
				}
			}
			// If the response has a JSON formatted error, get the message.
			if( err.responseText ){
				try{
					var res = JSON.parse(err.responseText);
					if( !res.error ){
						throw true;
					}
					msg = res.error;
				}catch(e){
					msg = err.responseText;
				}
			// Luck has run out for useful messages.
			}else{
				msg = err;
			}
			reject(Error(msg));
		});
	});
};

/*
### `authenticate`

Runs the preconfigured authenitcation function to login/fetch a valid API 
key to include in requests. Returns a promise to resolve the API key.
You shouldn't need to manually call this method, as it is called
automatically by `query`

```
pq.query('SELECT * FROM person WHERE age > $1', [50]).then(function(results){
	// do something with results
});
```
*/
Client.prototype.authenticate = function(){
	var client = this;
	return new Promise(function(resolve, reject){
		if( client.config.apiKey ){
			resolve(client.config.apiKey);
			return;
		}
		if( !client.config.authenticate ){
			reject(Error("No authentication function given"));
			return;
		}
		client.config.authenticate().then(function(apiKey){
			client.config.apiKey = apiKey;
			resolve(apiKey)
		},reject).catch(reject);
	});	
};

exports.Client = Client;