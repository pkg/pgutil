
var React = require('react/addons');
var Bacon = require('baconjs');
var uuid = require('node-uuid');
var jsonpatch = require('fast-json-patch'); 
var _ = require('lodash');

// React types are used for validation
var Types = React.PropTypes;
Types.json = Types.oneOfType([
	Types.array,
	Types.bool,
	Types.number,
	Types.string,
	Types.object
]);

var defaults = _.partialRight(_.assign, function(a, b) {
  return typeof a == 'undefined' ? b : a;
});

// Extend `target` with a config
function mixin(target, config){
	var mixins = [];
	if( config.mixins ){
		mixins = config.mixins;
		delete config.mixins;
	}
	_mixin(target, config);
	for(var i=0; i<mixins.length; i++){
		mixin(target, mixins[i]);
	}
	return target;
}

// Extend `target` with a mixin-less config
function _mixin(target, config){
	// merge inits
	if( target.init && config.init ){
		var init = target.init;
		target.init = function(){
			config.init.apply(this);
			init.apply(this);
		}
	}
	// extend missing
	return defaults(target, config);
}

// Normalize an argument to an array, by wrapping it
// in one if it is not one or using toArray if available.
function normalizeToArray(list){
	if( !_.isArray(list) ){
		if( list.toArray ){
			list = list.toArray();
		} else {
			list = [list];
		}
	}
	return list
}

// Flux namespace
var Flux = function(config){
	this.ActionBus = new Bacon.Bus();
	this.ErrorBus = new Bacon.Bus();
};

// Components and Data both have `on` and `assign` methods
// that can be used to respond to actions and stream changes
Flux.prototype.mixin = function(isComponent){
	var down = [], initialState = {};
	var mixin = {
		// Listen for changes to stream values
		on: function(stream, fn){
			if( !stream ){
				throw new Error('cannot add stream listener: missing stream object');
			}
			if( stream.toStream ){
				stream = stream.toStream();
			}
			if( !stream.onValue ){
				throw new Error('cannot add stream listener: invalid stream object');
			}
			if( !fn || typeof fn != 'function' ){
				throw new Error('cannot add stream listener: invalid listener function');
			}
			down.push( stream.onValue(fn) );
		},
		// Listen for stream values and assign the latest value to the state
		// at at the given name. Optionally set an initial value (default []) which
		// will be added to the getInitialState function.
		// The assigned value will be wrapped in lodash wrapper if it is a "collection"
		// as this is the most common use for `assign` within components. Call .value() to
		// get the underlying value.
		assign: function(stream, name, initialValue){
			var self = this;
			if( !stream ){
				throw new Error('cannot assign given stream to state: missing stream object');
			}
			if( stream.toStream ){
				stream = stream.toStream();
			}
			if( !stream.onValue ){
				throw new Error('cannot assign given stream to state: invalid stream object');
			}
			if( typeof initialValue == 'undefined' ){
				initialValue = [];
			}
			initialState[name] = initialValue;
			if( !(stream instanceof Bacon.Property) ){
				stream = stream.toProperty(initialValue);
			}
			down.push( stream.onValue(function(v){
				if( _.isArray(v) || _.isPlainObject(v) ){
					v = _(v);
				}
				// If component has "state" then assign to that
				if( self.setState ){
					var o = {};
					o[name] = v;
					self.setState(o);	
				}
				// Otherwise assign to "this" (for things like Data streams)
				else {
					self[name] = v;
				}
				
			}) );
		}
	};
	if( isComponent ){
		_.assign(mixin, {
			// Merge initial state
			getInitialState: function(){
				return initialState;
			},
			// Component setup
			componentWillMount: function(){
				initialState = {};
				if( this.init ){
					this.init();
				}
			},
			// Component teardown
			componentWillUnmount: function(){
				for(var i=0; i<down.length; i++){
					down[i]();
				}
				down = [];
			}
		});
	}
	return mixin;
}

/*
## Components

Components are Flux-flavored React Components.
Components have an `init` function that is run imediately and only once.
Components can listen for Actions using their `on` method.
Components can listen changes to data from a Stream or Store using their `assign` method.
*/
Flux.prototype.Component = function(config){
	config.mixins = config.mixins || [];
	config.mixins.push(this.mixin(true));
	var component = React.createClass(config);
	return component;
}

/*
## App

App is a special Component. It is the root component of the application.
App must only be defined once.
*/

Flux.prototype.App = function(config){
	if( this.root ){
		throw new Error('Flux.App has already been defined');
	}
	this.root = this.Component(config);
	return this.root;
}

// Shortcut to render the root node.
Flux.prototype.render = function(node){
	React.renderComponent(this.root({}), node || document.body);
}


/*
## Flux.Stream.

Streams are the building block of everything in Flux.

Values are added to the stream via `push`.

	```
	// A stream that outputs the value `'ping'` every 1s.
	var Pinger = Flux.Stream({
		init: function(){
			setInteval(function(){
				this.push('ping');
			}.bind(this), 1000)
		}
	})
	```

Values can be read from other streams by registering handlers using `on` or `assign` without affecting the stream itself.

	```
	var Log = Flux.Stream({
		init: function(){
			this.on(SomeOtherStream, this.log);
		},
		log: function(value){
			console.log('got value', value);
		}
	})
	```

Streams can be plugged into each other by calling `listen`.

	```
	var StreamA = Flux.Stream({});
	var StreamB = Flux.Stream({
		init: function(){
			this.listen(StreamB);
		}
	})
	// streamB will output both it's own values AND values from streamA
	```

Streams can be configured to validate their input payloads.

	```
	var MyStream = Flux.Stream({
		input: {
			name: Flux.Types.string.isRequired
		}
	})
	MyStream.push({name: 'bob'}); // OK
	MyStream.push({name: 1});     // Will cause an error
	```

Streams can be configured to tweak there output values.
This is just a convinience for a very common situation where you want a stream to give consistant output,
use `map`, `filter` and the combination methods for more advanced usage.

	```
	var NotStream = Flux.Stream({
		input: Flux.Types.bool.isRequired,
		output = function(input){
			return !input;
		}
	})
	MyStream.onValue(function(v){ alert(v) });
	MyStream.push(true); // alert's false
	```
*/
Flux.prototype.Stream = function(config){
	// create the underlying eventstream
	var _bus = new Bacon.Bus(),
		bus = _bus,
		errs;
	// Build the config
	var cfg = mixin({}, this.mixin());
	mixin(cfg, config);
	mixin(cfg, {
		kind: "Stream",
		listen: function(stream){
			if( !stream ){
				throw new Error('cannot listen to stream: stream is invalid');
			}
			if( stream.toStream ){
				stream = stream.toStream();
			}
			return _bus.plug(stream);
		},
		init: _.noop,
		plug: function(){
			return _bus.plug(stream);	
		},
		push: function(v){
			return _bus.push(v)
		},
		error: function(e){
			return _bus.error(e);
		}
	});
	// Validate inputs
	if( cfg.input ){
		// run react validator against input with config.input
		console.warn('input validator not implemented!');
	}
	// Simple internal output mapping
	if( cfg.output ){
		bus = bus.map(cfg.output);
	}
	// if this is a scanned stream convert to a property and re-extend
	if( cfg.scanner ){
		bus = bus.scan(cfg.state || {}, cfg.scanner.bind(bus));
	}
	// extend the bus with our cfg
	mixin(bus, cfg);
	bus.errors = function(){
		return errs;
	}
	bus.init();
	return bus;
}

/*
## Actions

Actions are a special kind of Stream wrapped in `Function` for communicating events within your app (mainly from Components to Stores).

	```
	var MyAction = Flux.Action({});
	MyAction({id: 1}); // emits {id: 1} on the MyAction stream
	```

Other Streams, Stores, Components etc can listen for Action values just like any other stream using `on`

	```
	var MyStream = Flux.Stream({
		init: function(){
			this.on(MyAction, this.onMyAction);
		},
		onMyAction: function(payload){
			console.log(payload);
		}
	});
	```

Actions can configure the `call` which is called to emit values to the Stream. The default `call` is 
just an alias for `push` but you can override it in `config`.

	```
	var SetAction = Flux.Action({
		call: function(id, values){
			this.push({
				id: id,
				values: 
			})
		}
	})
	SetAction(1, {name: 'bob'})
	```

Actions are added to the global Flux.ActionBus Stream which can be used to monitor all Action activity (useful for logging, stats or debugging).
*/
Flux.prototype.Action = function(config){
	var stream = this.Stream(mixin(config, {
		kind: "Action",
		call: function(payload){
			this.push(payload);
		}
	}));
	this.ActionBus.plug(stream.map(function(v){
		return {
			source: stream,
			value: v
		};
	}));
	var fn = function(){
		stream.call.apply(stream, arguments);
	};
	fn.toStream = function(){
		return stream;
	}
	return fn;
}

/*
## Stores

Stores are Streams with the concept of a current state and a set of Actions for modifiying that state.
Stores listen for a continuous stream of JSON patch (RFC6902) documents by listening to their associated action.
Stores output the latest version of the state on their outputs (`map` etc).
For best compatibility with `Components` the state should be a set of objects each keyed by a unique alphanumeric
key (a UUID is a good choice), but this is not enforced.

The following actions are created with each Store:

### store.Patch
### store.Create
### store.Update
### store.Delete


*/
Flux.prototype.Store = function(config){
	// `Create(o)` adds object `o` to the store.
	// Takes an object (or Array of objects) as input and generates output suitable for Patch
	// If the object has no id a UUIDv4 will be generated for you.
	var Create = this.Action({
		call: function(objects){
			if(!objects){
				return;
			}
			objects = normalizeToArray(objects);
			if( objects.length === 0 ){
				return;
			}
			this.push(_.reduce(objects, function(changeset, o){
				if( !o.id ){
					o.id = uuid.v4();
				}
				return [{op: 'add', path: ['/', o.id].join(''), value: o}];
			}, [], this));
		}
	});
	// `Update(objects, transaction)` executes the function `transaction` for each object in
	// `objects`. The `transaction` will be bound to an observed version of the object,
	// then records any changes made to the object during the transaction and emits 
	// the resulting changeset output suitable for Patch.
	// If `objects` is not an `Array` it will be just apply the update to the one object (ie you can
	// either pass an Array of objects or just the one).
	// If the object is not an object that originated from the Store or the patch cannot be created an 
	// error will be pushed in the stream.
	var Update = this.Action({
		call: function(objects, transaction){
			objects = normalizeToArray(objects);
			if( objects.length === 0 ){
				return;
			}
			try{
				this.push(_.reduce(objects, function(changeset, o){
					changeset.concat(this.patch(o, transaction));
					return changeset;
				},[], this));
			}catch(e){
				this.error(e);
			}
		},
		patch: function(o, transaction){
			if( !o.id ){
				throw new Error('cannot update object without an id');
			}
			var observee = _.cloneDeep(o); // probably a bottleneck here... check
			var observer = jsonpatch.observe(observee);
			try{
				transaction.apply(observee, [observee]);
				if( o.id !== observee.id ){
					throw new Error('id properties are imutable');
				}
				return _.map(jsonpatch.generate(observer), function(op){
					// patch was made against the object itself, we want it
					// against the Store's state, so shift the paths.
					op.path = ['/', o.id, op.path].join('');
					if(op.from){
						op.from = ['/', o.id, op.from].join('');
					}
					return op;
				})
			} finally {
				jsonpatch.unobserve(observee, observer);
			}
		}

	});
	// `Destroy(o)` removes object `o` from the Store.
	// Takes an object (or Array of objects) on input and generates output suitable for Patch 
	var Destroy = this.Action({
		call: function(objects){
			objects = normalizeToArray(objects);
			if( objects.length === 0 ){
				return;
			}
			try{
				this.push(_.reduce(objects, function(changeset, o){
					if( !o.id ){
						throw new Error('cannot destroy object without id');
					}
					return [{op: 'remove', path: ['/', o.id].join('')}];
				}, [], this));
			}catch(e){
				this.error(e);
			}
		}
	});
	// Takes JSON patch commands as input.
	var Patch = this.Action({
		init: function(){
			this.listen(Create);
			this.listen(Update);
			this.listen(Destroy);
		},
		input: Types.arrayOf(Types.shape({
			op: Types.oneOf(['add', 'remove', 'replace', 'move', 'copy', 'test']).isRequired,
			path: Types.string.isRequired,
			from: Types.string,
			value: Types.json,
		})),
	});
	return this.Stream(mixin(config, {
		kind: "Store",
		Create: Create,
		Update: Update,
		Destroy: Destroy,
		Patch: Patch,
		init: function(){
			this.listen(Patch);
		},
		// The state/scanner reduce the Stream to a single value (that changes over time)
		state: {},
		scanner: function(state, patch){
			if( !patch || patch.length === 0){
				return state;
			}
			if( !jsonpatch.apply(state, patch) ){
				console.error('jsonpatch failed returning error down the pipe state:', state, 'patch:', JSON.stringify(patch));
				return new Bacon.Error('patch could not be applied'); // is this correct?
			}
			return state;
		}
	}));
}

// Generate a UUIDv4
Flux.prototype.uuid = uuid.v4();

// React Aliases
Flux.prototype.React = React;
Flux.prototype.DOM = React.DOM;
Flux.prototype.Types = Types;
Flux.prototype.classSet = React.addons.classSet;

// Public API
module.exports = new Flux({});
