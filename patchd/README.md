patchd
======

patchd is patchwork's application server. It's main responsibilities include:

* Providing an HTTP interface to query the application state,
* Providing an HTTP interface to POST JSON-Patch documents that alter the state.
* Serving static assets.



