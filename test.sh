#!/bin/bash -e

set -e

DBNAME="pqutil_test"
DBARGS="${DBNAME}"

dropdb $DBARGS
createdb $DBARGS

cat << EOF | psql -d $DBNAME

	CREATE SCHEMA private;

	CREATE TABLE private.member (
		id SERIAL PRIMARY KEY, 
		name TEXT);

	CREATE TABLE private.organisation (
		id SERIAL PRIMARY KEY, 
		name TEXT);

	CREATE TABLE private.organisation_member (
		organisation_id INT REFERENCES private.organisation, 
		member_id INT REFERENCES private.member);

	CREATE TABLE private.document (
		id SERIAL PRIMARY KEY, 
		organisation_id INT REFERENCES private.organisation, 
		name TEXT);

	INSERT INTO private.member (name) VALUES ('alice');
	INSERT INTO private.member (name) VALUES ('bob');
	INSERT INTO private.member (name) VALUES ('jeff');

	INSERT INTO private.organisation (name) VALUES ('SuperGloboCorp');
	INSERT INTO private.organisation (name) VALUES ('MegaInc');

	INSERT INTO private.organisation_member (member_id, organisation_id) VALUES (1,1);
	INSERT INTO private.organisation_member (member_id, organisation_id) VALUES (2,1);
	INSERT INTO private.organisation_member (member_id, organisation_id) VALUES (3,2);

	INSERT INTO private.document (name, organisation_id) VALUES ('Secret SuperGloboCorp document 1', 1);
	INSERT INTO private.document (name, organisation_id) VALUES ('Secret SuperGloboCorp document 2', 1);
	INSERT INTO private.document (name, organisation_id) VALUES ('Secret MegaInc document 1', 2);

EOF

(cd parser && make) && go test ./...
